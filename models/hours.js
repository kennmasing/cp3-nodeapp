//DECLARE DEPENDENCIES
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");
// const uniqueValidator = require("mongoose-unique-validator");

const hourSchema = new Schema(
	{
		duration: {
			type: Number
		},
		rate: {
			type: Number
		}
	},
	{
		timestamps: true,
	}
);

//(OPTIONAL) SET RELATIONSHIP
hourSchema.virtual("bookings", {
	ref: "Booking",
	localField: "_id",
	foreignField: "hourId"
});



const Hour = mongoose.model("Hour", hourSchema);

module.exports = Hour;
