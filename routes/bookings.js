const Booking = require("../models/bookings");
// const Parking = require("../models/parkings");
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");

//---------BOOKINGS-----------
//CREATE NEW BOOKING
router.post("/", auth, async (req, res) => {
	const booking = new Booking({
		...req.body,
		userId: req.user._id,
		locationId: req.body.locationId,
		buildingId: req.body.buildingId,
		parkingId: req.body.parkingId,
		startDate: req.body.startDate,
		endDate: req.body.endDate
	});

	try {
		await booking.save();
		res.status(201).send(booking);
	} catch(e) {
		res.status(400).send(e);
	}
});

//GET ALL BOOKINGS
router.get("/", auth, async (req, res) => {
	try {
		// const now = new Date()
		// console.log(now)
		// const filter = await Booking.updateMany({isCompleted: false, endDate: {$lte:now}}, {$set: {isCompleted: true}})

		const bookings = await Booking.find(req.query)
			.populate({ path: 'userId', select: ['firstName', 'lastName', 'email', 'roleId'] })
			.populate({ path: 'locationId', select: 'name' })
			.populate({ path: 'buildingId', select: 'name' })
			.populate({ path: 'parkingId', select: ['name','isAvailable'] })
			.populate({ path: 'statusId', select: ['name', 'statusCode'] })
			.exec()
		return res.status(200).send(bookings)
	} catch(e) {
		return res.status(404).send(e);
	}
});

//GET ALL BOOKINGS
router.get("/history", auth, async (req, res) => {
	
	let where = {}
	let limit = 0
	let skip = 0

	if(req.query.isPaid == "true") {
		where = { isPaid : true }
	} else if(req.query.isPaid == "false") {
		where = { isPaid : false }
	}

	if(req.query.isCompleted == "true") {
		where = { isCompleted : true }
	} else if(req.query.isCompleted == "false") {
		where = { isCompleted : false }
	}

	if(req.query.isArchived == "true") {
		where = { isArchived : true }
	} else if(req.query.isArchived == "false") {
		where = { isArchived : false }
	}

	if(req.query.limit) {
		limit = parseInt(req.query.limit)
	}

	if(req.query.skip) {
		skip = parseInt(req.query.skip)
	}

	try {
		// const now = new Date()
		// console.log(now)
		// const filter = await Booking.updateMany({isCompleted: false, endDate: {$lte:now}}, {$set: {isCompleted: true}})

		const bookings = await Booking.find(where)
			.limit(limit)
			.skip(skip)
			.populate({ path: 'userId', select: ['firstName', 'lastName', 'email', 'roleId'] })
			.populate({ path: 'locationId', select: 'name' })
			.populate({ path: 'buildingId', select: 'name' })
			.populate({ path: 'parkingId', select: ['name','isAvailable'] })
			.populate({ path: 'statusId', select: ['name', 'statusCode'] })
			.exec()
			// .limit(limit)
			// .skip(skip)

		const total = await Booking.estimatedDocumentCount();

		return res.status(200).send({total, bookings})

		// return res.status(200).send(bookings)

	} catch(e) {
		return res.status(404).send(e);
	}
});


//GET ALL BOOKINGS WITH FILTER
// router.get("/", auth, async (req, res) => {
// 	const match = {}
// 	const sort = {}

// 	if(req.query.isCompleted) {
// 		match.isCompleted = req.query.isCompleted
// 	}

// 	if(req.query.sortBy) {
// 		const parts = req.query.sortBy.split(":");
// 		sort[parts[0]] = parts[1] === "desc" ? -1 : 1
// 	}

// 	try {
// 		await req.user.populate({
// 			path: "bookings",
// 			match,
// 			options: {
// 				limit: parseInt(req.query.limit),
// 				skip: parseInt(req.query.skip),
// 				sort
// 			}
// 		})
// 		.execPopulate()

// 		return res.status(200).send(req.user.bookings)
// 	} catch(e) {
// 		return res.status(404).send(e);
// 	}
// });

//GET ONE BOOKING
router.get("/:id", auth, async (req, res) => {
	console.log("GET ONE BOOKING")
	try {
		const booking = await Booking.findById(req.params.id)
			.populate({ path: 'userId', select: ['firstName', 'lastName', 'email', 'roleId'] })
			.populate({ path: 'locationId', select: 'name' })
			.populate({ path: 'buildingId', select: 'name' })
			.populate({ path: 'parkingId', select: ['name','isAvailable'] })
			.populate({ path: 'statusId', select: ['name', 'statusCode'] })
			.exec()
		return res.status(200).send(booking)
	} catch(e) {
		return res.status(404).send(e);
	}
});

//GET ALL BOOKINGS
router.get("/:id/me", auth, async (req, res) => {
	let where = {userId: req.params.id}
	let limit = 0
	let skip = 0

	if(req.query.isPaid == "true") {
		where = { isPaid : true }
	} else if(req.query.isPaid == "false") {
		where = { isPaid : false }
	}

	if(req.query.isCompleted == "true") {
		where = { isCompleted : true }
	} else if(req.query.isCompleted == "false") {
		where = { isCompleted : false }
	}

	if(req.query.isArchived == "true") {
		where = { isArchived : true }
	} else if(req.query.isArchived == "false") {
		where = { isArchived : false }
	}

	if(req.query.limit) {
		limit = parseInt(req.query.limit)
	}

	if(req.query.skip) {
		skip = parseInt(req.query.skip)
	}

	try {
		// const now = new Date()
		// console.log(now)
		// const filter = await Booking.updateMany({isCompleted: false, endDate: {$lte:now}}, {$set: {isCompleted: true}})

		const bookings = await Booking.find(where)
			.limit(limit)
			.skip(skip)
			.populate({ path: 'userId', select: ['firstName', 'lastName', 'email', 'roleId'] })
			.populate({ path: 'locationId', select: 'name' })
			.populate({ path: 'buildingId', select: 'name' })
			.populate({ path: 'parkingId', select: ['name','isAvailable'] })
			.populate({ path: 'statusId', select: ['name', 'statusCode'] })
			.exec()
			// .limit(limit)
			// .skip(skip)

		const total = await Booking.estimatedDocumentCount();

		return res.status(200).send({total, bookings})
	} catch(e) {
		return res.status(404).send(e);
	}
});

router.get("/:id/parking", auth, async (req, res) => {
	try {
		// const now = new Date()
		// console.log(now)
		// const filter = await Booking.updateMany({isCompleted: false, endDate: {$lte:now}}, {$set: {isCompleted: true}})

		const bookings = await Booking.find({parkingId: req.params.id})
			.populate({ path: 'userId', select: ['firstName', 'lastName', 'email', 'roleId'] })
			.populate({ path: 'locationId', select: 'name' })
			.populate({ path: 'buildingId', select: 'name' })
			.populate({ path: 'parkingId', select: ['name','isAvailable'] })
			.populate({ path: 'statusId', select: ['name', 'statusCode'] })
			.exec()
		return res.status(200).send(bookings)
	} catch(e) {
		return res.status(404).send(e);
	}
});

//GET ALL BOOKINGS WITH SPECIFIC PARKING ID
// router.get("/:id", auth, async (req, res) => {
// 	try {
// 		const bookings = await Booking.find({parkingId: req.params.id})
// 			// .populate({ path: 'userId', select: 'email' })
// 			// .populate({ path: 'locationId', select: 'name' })
// 			// .populate({ path: 'buildingId', select: 'name' })
// 			// .populate({ path: 'parkingId', select: ['name','isAvailable'] })
// 			// .populate({ path: 'statusId', select: 'name' })
// 			// .exec()
// 		return res.status(200).send(bookings)
// 	} catch(e) {
// 		return res.status(404).send(e);
// 	}
// });




//UPDATE A BOOKING
router.patch("/:id", auth, async (req, res) => {
	const updates = Object.keys(req.body)

	const allowedUpdates = [
		"nameId",
		"parkingId",
		"hourId",
		"statusId",
		"isPaid",
		"isCompleted",
		"isArchived",
	]

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, req.body, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//PAY A BOOKING
router.patch("/:id/paid", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isPaid"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {isPaid: true}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//PAY A BOOKING
router.patch("/:id/unpaid", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isPaid"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {isPaid: false}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//PENDING A BOOKING
router.patch("/:id/pending", auth, async (req, res) => {	
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"statusId"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		// const booking = await Booking.findByIdAndUpdate(req.params.id, {statusId: "5e5bd66b4010b83ad0dcd3b3"}, { new: true }); //LOCAL DB
		const booking = await Booking.findByIdAndUpdate(req.params.id, {statusId: "5e5fac721c84eb4bf42d4879"}, { new: true }); //REMOTE DB
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//APPROVE A BOOKING
router.patch("/:id/approve", auth, async (req, res) => {	
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"statusId"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		// const booking = await Booking.findByIdAndUpdate(req.params.id, {statusId: "5e5bd67a4010b83ad0dcd3b4"}, { new: true }); //LOCAL DB
		const booking = await Booking.findByIdAndUpdate(req.params.id, {statusId: "5e5fac7f1c84eb4bf42d487a"}, { new: true }); //REMOTE DB
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//DECLINE A BOOKING
router.patch("/:id/decline", auth, async (req, res) => {	
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"statusId"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		// const booking = await Booking.findByIdAndUpdate(req.params.id, {statusId: "5e5bd6844010b83ad0dcd3b5"}, { new: true }); //LOCAL DB
		const booking = await Booking.findByIdAndUpdate(req.params.id, {statusId: "5e5fac881c84eb4bf42d487b"}, { new: true }); //REMOTE DB
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//COMPLETE A BOOKING
router.patch("/:id/completed", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isCompleted"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {isCompleted: true}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//ONGOING A BOOKING
router.patch("/:id/ongoing", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isCompleted"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {isCompleted: false}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})


//ARCHIVE A BOOKING
router.patch("/:id/archive", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isArchived"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {isArchived: true}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//UNARCHIVE A BOOKING
router.patch("/:id/unarchive", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isArchived"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {isArchived: false}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//PAY A BOOKING
router.patch("/:id/parking", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"ParkingId"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const booking = await Booking.findByIdAndUpdate(req.params.id, {parkingId: req.params.id}, { new: true });
		if(!booking) {
			return res.status(404).send("Booking doesn't exist!")
		}
		return res.send(booking);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//CANCEL A BOOKING
router.delete("/:id/cancel", auth, async(req, res) => {
	try {
		const booking = await Booking.findByIdAndDelete(req.params.id)
		res.send(booking);
	} catch(e) {
		res.status(500).send(e)
	}
})

//DELETE BOOKING
// router.delete("/:id", auth, async(req, res) => {
// 	try {
// 		const booking = await Booking.findByIdAndDelete(req.params.id)

// 		res.send(booking);
// 	} catch(e) {
// 		res.status(500).send(e)
// 	}
// })

// //EXPORT ROUTER
module.exports = router;
