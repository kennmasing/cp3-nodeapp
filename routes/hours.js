const Hour = require("../models/hours");
// const Parking = require("../models/parkings");
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");

//---------hourS-----------
//CREATE hour
router.post("/", auth, async (req, res) => {
	console.log("Create new hour")

	const hour = new Hour(req.body);

	try {
		await hour.save();
		res.status(201).send(hour);
	} catch(e) {
		res.status(400).send(e);
	}
});

//GET ALL hourS
router.get("/", auth, async (req, res) => {
	console.log("GET HOURS")
	try {
		const hours = await Hour.find();
		return res.status(200).send(hours)
	} catch(e) {
		return res.status(404).send(e);
	}
});

//GET ONE
router.get("/:id", async (req, res) => {
	try {
		const hour = await Hour.findById(req.params.id)
		
		if(!hour) {
			return res.status(404).send("hour not found")
		}
		return res.status(200).send(hour)
	} catch(e) {
		return res.status(500).send(e);
	}
});

// GET ALL hourS WITH FILTER
// router.get("/", auth, async (req, res) => {
// 	const match = {}
// 	const sort = {}

// 	if(req.query.isCompleted) {
// 		match.isCompleted = req.query.isCompleted
// 	}

// 	if(req.query.sortBy) {
// 		const parts = req.query.sortBy.split(":");
// 		sort[parts[0]] = parts[1] === "desc" ? -1 : 1
// 	}

// 	try {
// 		await req.user.populate({
// 			path: "hours",
// 			match,
// 			options: {
// 				limit: parseInt(req.query.limit),
// 				skip: parseInt(req.query.skip),
// 				sort
// 			}
// 		})
// 		.execPopulate()

// 		return res.status(200).send(req.user.hours)
// 	} catch(e) {
// 		return res.status(404).send(e);
// 	}
// });

//UPDATE A Building
router.patch("/:id", auth, async (req, res) => {
	console.log("Update a hour")	
	const updates = Object.keys(req.body)

	const allowedUpdates = [
		"duration",
		"rate"
	]

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
}

	try {
		const hour = await Hour.findByIdAndUpdate(req.params.id, req.body, { new: true });
		if(!hour) {
			return res.status(404).send("Building doesn't exist!")
		}
		return res.send(hour);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//DELETE hour
router.delete("/:id", auth, async(req, res) => {
	console.log("delete hour")
	try {
		const hour = await Hour.findByIdAndDelete(req.params.id)

		res.send(hour);
	} catch(e) {
		res.status(500).send(e)
	}
})

// //EXPORT ROUTER
module.exports = router;
