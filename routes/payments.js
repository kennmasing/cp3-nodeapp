const express = require("express");
const router = express.Router();
const stripe = require('stripe')("sk_test_V9ydeic0207YnYDkdreYLLiB00SbP3c7DE");
const sendgrid = require("../middleware/sendgrid.js")

router.post("/", (req, res) => {
    
    const stripeChargeCallback = res => (stripeErr, stripeRes) => {
        if(stripeErr){
            res.status(500).send({ error: stripeErr })
        } else {
            res.status(200).send({ success: stripeRes })
        }
    }

    const body = {
        source: req.body.token.id,
        amount: req.body.amount,
        email: req.body.email,
        currency: "PHP"
    }

    stripe.charges.create(body, stripeChargeCallback(res));
})

module.exports = router;