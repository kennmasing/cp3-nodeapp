const Status = require("../models/statuses");
// const Parking = require("../models/parkings");
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");

//---------statusS-----------
//CREATE status
router.post("/", auth, async (req, res) => {
	console.log("Create new status")

	const status = new Status({
		...req.body,
		userId: req.user._id
		// parkingId: parking._id
	});

	try {
		await status.save();
		res.status(201).send(status);
	} catch(e) {
		res.status(400).send(e);
	}
});

//GET ALL statusS
router.get("/", auth, async (req, res) => {
	console.log("GET statusS")
	try {
		const statuses = await Status.find();
		return res.status(200).send(statuses)
	} catch(e) {
		return res.status(404).send(e);
	}
});

//GET ALL statusS WITH FILTER
// router.get("/", auth, async (req, res) => {
// 	const match = {}
// 	const sort = {}

// 	if(req.query.isCompleted) {
// 		match.isCompleted = req.query.isCompleted
// 	}

// 	if(req.query.sortBy) {
// 		const parts = req.query.sortBy.split(":");
// 		sort[parts[0]] = parts[1] === "desc" ? -1 : 1
// 	}

// 	try {
// 		await req.user.populate({
// 			path: "statuss",
// 			match,
// 			options: {
// 				limit: parseInt(req.query.limit),
// 				skip: parseInt(req.query.skip),
// 				sort
// 			}
// 		})
// 		.execPopulate()

// 		return res.status(200).send(req.user.statuses)
// 	} catch(e) {
// 		return res.status(404).send(e);
// 	}
// });

//GET ONE status
router.get("/:id", async (req, res) => {
	console.log("Get one status")
	try {
		const status = await Status.findById(req.params.id)
		return res.status(200).send(status)
	} catch(e) {
		return res.status(404).send(e);
	}
})


//UPDATE A Building
router.patch("/:id", auth, async (req, res) => {
	console.log("Update a status")	
	const updates = Object.keys(req.body)

	const allowedUpdates = [
		"name",
		"statusCode"
	]

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
}

	try {
		const status = await Status.findByIdAndUpdate(req.params.id, req.body, { new: true });
		if(!status) {
			return res.status(404).send("status doesn't exist!")
		}
		return res.send(status);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//DELETE status
router.delete("/:id", auth, async(req, res) => {
	console.log("delete status")
	try {
		const status = await Status.findByIdAndDelete(req.params.id)

		res.send(status);
	} catch(e) {
		res.status(500).send(e)
	}
})

// //EXPORT ROUTER
module.exports = router;
