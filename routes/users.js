//DECLARE DEPENDENCIES
const User = require("../models/users");
const express = require("express");
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require("../middleware/auth");
const multer = require("multer");
const sharp = require("sharp");
const sendgrid = require("../middleware/sendgrid")


//-----CREATE NEW ROUTES-----
//(1) CREATE NEW USER
router.post("/", async (req, res) => {
	console.log("Create a user", req.body)

	const user = new User(req.body);

	try {
		await user.save();
		console.log("MEMBER USERNAME", user.username)
		console.log("MEMBER EMAIL", user.email)
		res.status(201).send(user);
		sendgrid.sendRegisterConfirmation(user.username, user.email)
	} catch(e) {
		res.status(400).send(e.message);
	}
});

//(3) GET ALL USERS
router.get("/", auth, async (req, res) => {
	try {
		const users = await User.find();
		return res.status(200).send(users)
	} catch(e) {
		return res.status(404).send(e.message);
	}
})

//(3) GET CURRENT USER
router.get("/me", auth, async (req, res) => {
	console.log(req.user)
	try {
		const user = await User.findById(req.user._id);
		return res.status(200).send(user)
	} catch(e) {
		return res.status(404).send(e.message);
	}
})

//(3) GET ONE
router.get("/:id", auth, async (req, res) => {
	console.log(req.params.id)
	try {
		const user = await User.findById(req.params.id);
		return res.status(200).send(user)
	} catch(e) {
		return res.status(404).send(e.message);
	}
})

//(D) LOGIN
router.post("/login", async(req, res) => {

	try {
		let user = "";
		if( !req.body.username ) {
			user = await User.findOne({ email: req.body.email });	
			console.log(req.body.email);
		} else {
			console.log(req.body.username);
			user = await User.findOne({ username: req.body.username });	
		}

		if(!user) {
			return res.status(404).send({"message" : "Invalid login credentials YYYY!"})
		}

		// //comparing req pw vs unhashed db password
		const isMatch = await bcrypt.compare(req.body.password, user.password);

		if(!isMatch) {
			return res.status(500).send({"message" : "Invalid login credentials XXXXX!"})
		}

		//GENERATE A TOKEN
		const token = await user.generateAuthToken();

		//S04 CONNECTING TO BACKEND
		console.log(user);

		res.send({user, token});
	} catch(e) {
		
		//S04 CONNECTING TO BACKEND
		console.log(e)
		res.status(500).send(e);
	}
});

//(STRETCH GOAL) LOGOUT OF ONE DEVICE
router.post('/logout', auth, async (req, res) => {
  	console.log(req.user)
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save()
        res.send(req.user);

    } catch (e) {
        res.status(500).send(e)
    }
})

//(ACTIVITY 5) LOGOUT OF ALL DEVICES
router.post('/logoutAll', auth, async (req, res) => {
 console.log(req.user)
 try {
        req.user.tokens.splice(0, req.user.tokens.length)
        await req.user.save()
        res.send("You've been successfully logged out of all devices!")

    } catch (e) {
        res.status(500).send(e)
    }

});

// UPDATE ONE
router.patch("/:id", auth, async (req, res) => {

	const updates = Object.keys(req.body)

	const allowedUpdates = [
	"firstName",
	"lastName",
	"roleId",
	"isArchived"
	]

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}

	try {
		const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
		if(!user) {
			return res.status(404).send("User doesn't exist!");
		}
		return res.send(user);
	} catch(e) {
		res.status(500).send(e.message);
	}
});

//ARCHIVE A USER
router.patch("/:id/archive", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isArchived"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const user = await User.findByIdAndUpdate(req.params.id, {isArchived: true}, { new: true });
		if(!user) {
			return res.status(404).send("user doesn't exist!")
		}
		return res.send(user);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//UNARCHIVE A user
router.patch("/:id/unarchive", auth, async (req, res) => {
	const updates = Object.keys(req.body)
	const allowedUpdates = [
		"isArchived"
	]
	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))
	if(!isValidUpdate) {
		return res.status(400).send({error : "Invalid update!"})
	}
	try {
		const user = await User.findByIdAndUpdate(req.params.id, {isArchived: false}, { new: true });
		if(!user) {
			return res.status(404).send("user doesn't exist!")
		}
		return res.send(user);
	} catch(e) {
		return res.status(500).send(e)
	}
})

//SOFT DELETE A USER
router.delete("/:id/delete", async (req, res) => {
	console.log("Delete a user")
	try {
		const user = await User.findByIdAndDelete(req.params.id)
		return res.status(200).send(user)
	} catch(e) {
		return res.status(500).send(e.message)
	}
})


//EXPORT ROUTER
module.exports = router;
